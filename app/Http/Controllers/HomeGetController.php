<?php

namespace App\Http\Controllers;

use App\Ayarlar;
use App\Blog;
use App\Forum;
use App\ForumListe;
use App\Hakkimizda;
use App\Kategori;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeGetController extends HomeController
{
    public function get_index()
    {
        return view('frontend.index');
    }

    public function get_login()
    {
        return view('frontend.giris-yap');
    }

    public function get_logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function get_index_yonlendir()
    {
        return redirect('/');
    }

    public function get_iletisim()
    {
        $ayarlar = Ayarlar::where('ayar_id', 1)->select('ayarlar.*')->first();
        return view('frontend.iletisim')->with('ayarlar', $ayarlar);
    }

    public function get_hakkimizda()
    {
        $hakkimizda = Hakkimizda::where('id', 1)->select('hakkimizda.*')->first();
        return view('frontend.hakkimizda')->with('hakkimizda', $hakkimizda);
    }

    public function get_blog()
    {
        $hakkimizda=Hakkimizda::where('id',1)->select('hakkimizda.*')->first();
        $kategoriler = Kategori::where('ust_kategori', 0)->get();
        $bloglar = Blog::orderBy('id', 'desc')->get();
        return view('frontend.blog')->with('bloglar', $bloglar)->with('kategoriler', $kategoriler)->with('hakkimizda',$hakkimizda);
    }

    public function get_blog_icerik($slug)
    {
        $hakkimizda=Hakkimizda::where('id',1)->select('hakkimizda.*')->first();
        $kategori = explode('/', $slug);
        $kategoriler = Kategori::where('ust_kategori', 0)->get();
        $blog = Blog::where('slug', $kategori[count($kategori) - 1])->first();
        if (isset($blog)) {
            return view('frontend.blog-detay')->with('blog', $blog)->with('kategoriler', $kategoriler)->with('blog-kategori', $kategori)->with('hakkimizda',$hakkimizda);
        } else {
            $s = $kategori[count($kategori) - 1];
            $b = Kategori::where('slug', $s)->get();
            $bloglar = $b[0]->bloglar;
            return view('frontend.blog')->with('bloglar', $bloglar)->with('kategoriler', $kategoriler)->with('hakkimizda',$hakkimizda);
        }
    }

    public function get_blog_yazar($yazar)
    {
        $hakkimizda=Hakkimizda::where('id',1)->select('hakkimizda.*')->first();
        $url = explode('-', $yazar);
        $kategoriler = Kategori::where('ust_kategori', 0)->get();
        $bloglar = Blog::where('yazar', $url[count($url) - 1])->orderBy('id', 'desc')->get();
        return view('frontend.blog')->with('bloglar', $bloglar)->with('kategoriler', $kategoriler)->with('hakkimizda',$hakkimizda);

    }

    public function get_blog_tag($tag)
    {
        $hakkimizda=Hakkimizda::where('id',1)->select('hakkimizda.*')->first();
        $kategori = explode('-', $tag);
        $kategoriler = Kategori::where('ust_kategori', 0)->get();
        $bloglar = Blog::where('etiketler', 'like', '%' . $kategori[count($kategori) - 2] . '%')->orderBy('id', 'desc')->get();
        return view('frontend.blog')->with('bloglar', $bloglar)->with('kategoriler', $kategoriler)->with('hakkimizda',$hakkimizda);

    }

    public function get_app()
    {
        $user = User::where('id', Auth::user())->get();
        return view('frontend.app')->with('user', $user);
    }

    public function get_forum()
    {
        $hakkimizda=Hakkimizda::where('id',1)->select('hakkimizda.*')->first();
        $categories=Forum::all();
        $konular = Forum::orderBy('id', 'DESC')->get();
        return view('frontend.forum')->with('konular', $konular)->with('categories',$categories)->with('hakkimizda',$hakkimizda);
    }

    public function get_forum_liste($slug)
    {
        $hakkimizda=Hakkimizda::where('id',1)->select('hakkimizda.*')->first();
        $categories=Forum::all();
        $konular = Forum::where('slug', $slug)->first();
        $id = $konular->id;
        $altkonular = ForumListe::where('forum', $id)->get();
        return view('frontend.forum-liste')->with('konular', $altkonular)->with('categories',$categories)->with('anakonu',$konular)->with('hakkimizda',$hakkimizda);
    }

    public function get_tag_forum($tag)
    {
        $hakkimizda=Hakkimizda::where('id',1)->select('hakkimizda.*')->first();
        $categories=Forum::all();
        $kategori = explode('-', $tag);
        $konular = ForumListe::where('etiketler', 'like', '%' . $kategori[count($kategori) - 1] . '%')->orderBy('id', 'desc')->get();
        return view('frontend.forum-liste')->with('konular', $konular)->with('categories',$categories)->with('hakkimizda',$hakkimizda);

    }

    public function get_forum_konu_ekle()
    {
        $anakonu = Forum::all();
        return view('frontend.konu-ekle')->with('anakonular', $anakonu);
    }

    public function get_forum_detay($ana_baslik, $slug)
    {
        $hakkimizda=Hakkimizda::where('id',1)->select('hakkimizda.*')->first();
        $categories=Forum::all();
        $forum = ForumListe::where('slug', $slug)->first();
        return view('frontend.forum-detay')->with('forum', $forum)->with('categories',$categories)->with('hakkimizda',$hakkimizda);
    }

    public function get_author_forum($author)
    {
        $hakkimizda=Hakkimizda::where('id',1)->select('hakkimizda.*')->first();
        $categories=Forum::all();
        $user=Auth::user()->id;
        $konular=ForumListe::where('yazar',$user)->get();
        return view('frontend.forum-liste')->with('konular',$konular)->with('categories',$categories)->with('hakkimizda',$hakkimizda);
    }



}
