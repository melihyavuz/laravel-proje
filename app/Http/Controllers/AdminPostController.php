<?php

namespace App\Http\Controllers;

use App\Ayarlar;
use App\Blog;
use App\Forum;
use App\Hakkimizda;
use App\Kategori;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Image;

class AdminPostController extends AdminController
{
    public function post_ayarlar(Request $request)
    {
        if (isset($request->ayar_logo)) {
            $validator = Validator::make($request->all(), [
                'logo' => 'mimes:jpg,jpeg,png,gif'
            ]);

            if ($validator->fails()) {
                return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Yüklediğiniz dosya uzantısı jpg,jpeg,png,gif bunlardan biri olmalıdır.']);
            }
            //Gelen resmi alalım
            $logo = Input::file('ayar_logo');
            //dosyanın uzantısını almak için
            $logo_uzanti = Input::file('ayar_logo')->getClientOriginalExtension();
            //her resim yüklediğimizde logo/uzantı şeklinde olacak
            $logo_isim = 'ayar_logo' . $logo_uzanti;
            //makedirectory ile klasör oluşturcaz.yüklemek istediğimiz klasör yoksa bu metot otomatik oluşturcak.o klasöre kaydedecek.
            Storage::disk('uploads')->makeDirectory('img');
            //getrealpath dosyanın disk üzerindeki gerçek yolunu alır ve kaydını yapar.
            Image::make($logo->getRealPath())->resize(222, 108)->save('uploads/img/' . $logo_isim);
        }
        try {
            unset($request['_token']);
            if (isset($request->ayar_logo)) {
                unset($request['eski_logo']);
                Ayarlar::where('ayar_id', 1)->update($request->all());
                Ayarlar::where('ayar_id', 1)->update(['ayar_logo' => $logo_isim]);
            } else {
                $eski_logo = $request->eski_logo;
                unset($request['eski_logo']);
                Ayarlar::where('ayar_id', 1)->update($request->all());
                Ayarlar::where('ayar_id', 1)->update(['ayar_logo' => $eski_logo]);
            }
            return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Kayıt başarıyla yapıldı']);
        } catch (\Exception $e) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Kayıt yapılamadı']);
        }
    }

    public function post_hakkimizda(Request $request)
    {

        try {
            unset($request['_token']);
            Hakkimizda::where('id', 1)->update($request->all());
            return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Kayıt başarıyla yapıldı']);
        } catch (\Exception $e) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Kayıt yapılamadı']);
        }
    }

    public function post_blog_ekle(Request $request)
    {

            $validator = Validator::make($request->all(), [
                'resimler'=>'required',
                'resimler.*'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'baslik' => 'required|max:250',
                'etiketler' => 'required|max:250',
                'icerik' => 'required',
                'kisaicerik' => 'required:max:300',
                'kategori' => 'required'
            ]);


            if ($validator->fails()) {
                return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Doldurulması zorunlu olan alanları doldurun.']);
            }

        $tarih = str_slug(Carbon::now());
        $slug = str_slug($request->baslik) . '-' . $tarih;
        $resimler = $request->file('resimler');
        if (!empty($resimler)) {
            $i = 0;
            foreach ($resimler as $resim) {
                $resim_uzanti = $resim->getClientOriginalExtension();
                $resim_isim = $i . '.' . $resim_uzanti;
                Storage::disk('uploads')->makeDirectory('img/blog/' . $slug);
                Storage::disk('uploads')->put('img/blog/' . $slug . '/' . $resim_isim, file_get_contents($resim));
                $i++;
            }
        }
        try {
            $request->merge(['slug' => $slug]);
            Blog::create($request->all());
            return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Kayıt başarıyla yapıldı']);
        } catch (\Exception $e) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Kayıt yapılamadı']);
        }

    }

    public function post_blog_sil(Request $request)
    {
        try {
            Blog::where('slug', $request->slug)->delete();
            Storage::disk('uploads')->deleteDirectory('img/blog/' . $request->slug);
            return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Kayıt başarıyla silindi.']);
        } catch (\Exception $e) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Kayıt silinemedi.', 'hata' => $e]);
        }
    }

    public function post_blog_duzenle($slug, Request $request)
    {
        if (isset($request->resimler)) {
            $validator = Validator::make($request->all(), [
                'baslik' => 'required|max:250',
                'etiketler' => 'required|max:250',
                'icerik' => 'required',
                'kisaicerik' => 'required|max:300'
            ]);
            if ($validator->fails()) {
                return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Doldurulması zorunlu olan alanları doldurun']);
            }
        }
        if (isset($request->resim)) {
            try {
                Storage::disk('uploads')->delete($request->resim);
                return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Resim başarıyla silindi.']);

            } catch (\Exception $e) {
                return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Resim silinemedi.', 'hata' => $e]);
            }
        } else {
            $resimler = $request->file('resimler');
            if (!empty($resimler)) {
                $i = $request->sayi;
                foreach ($resimler as $resim) {
                    $resim_uzanti = $resim->getClientOriginalExtension();
                    $resim_isim = $i . '.' . $resim_uzanti;
                    Storage::disk('uploads')->makeDirectory('img/blog/' . $slug);
                    Storage::disk('uploads')->put('img/blog/' . $slug . '/' . $resim_isim, file_get_contents($resim));
                    $i++;
                }
            }
            try {
                Blog::where('slug', $slug)->update(['baslik' => $request->baslik, 'etiketler' => $request->etiketler, 'icerik' => $request->icerik, 'kisaicerik' => $request->kisaicerik]);
                return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Kayıt başarıyla güncellendi']);
            } catch (\Exception $e) {
                return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Kayıt yapılamadı']);
            }
        }
    }

    public function post_kategori_ekle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ad' => 'required'
        ]);
        if ($validator->fails()) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Ad alanı boş bırakılamaz']);
        }
        try {
            $slug = str_slug($request->ad);
            $request->merge(['slug' => $slug]);
            Kategori::create($request->all());
            return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Kayıt başarıyla güncellendi']);
        } catch (\Exception $e) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Kayıt yapılamadı']);
        }
    }

    public function post_kategori_sil(Request $request)
    {
        try {
            Kategori::where('id', $request->id)->delete();
            return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Silme işlemi başarılı']);
        } catch (\Exception $e) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Kayıt silinemedi.', 'hata' => $e]);
        }
    }
    public function post_forum(Request $request){
        $validator=Validator::make($request->all(),[
            'baslik'=>'required|max:250',
            'kisa_aciklama'=>'required|max:250',

        ]);
        if($validator->fails()){
            return response(['durum'=>'error','baslik'=>'Hatalı','icerik'=>'Alanları Tekrar Gözden Geçiriniz.']);
        }
        try{
            $slug=str_slug($request->baslik);
            $request->merge(['slug'=>$slug]);
            Forum::create($request->all());
            return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Kayıt başarıyla yapıldı']);
        } catch (\Exception $e) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Kayıt yapılamadı', 'hata' => $e]);
        }
    }
}
