<?php

namespace App\Http\Controllers;


use App\Blog;
use App\ForumListe;
use App\ForumYorumlar;
use App\Yorum;
use Carbon\Carbon;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomePostController extends HomeController
{

    public function post_blog_yorum($slug, Request $request)
    {
        if (Auth::check()) {
            $validator = Validator::make($request->all(), [
                'icerik' => 'required'
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'isim' => 'required',
                'mail' => 'required|email',
                'icerik' => 'required'
            ]);
        }
        if ($validator->fails()) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Yüklediğiniz dosya uzantısı jpg,jpeg,png,gif bunlardan biri olmalıdır.']);
        }
        $kategori = explode('/', $slug);
        if (Auth::check()) {
            $request->merge(['kullanici_id' => Auth::user()->id]);
        }
        $request->merge(['blog' => $kategori[count($kategori) - 1]]);
        Yorum::create($request->all());
        return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Kayıt başarıyla yapıldı']);

    }

    public function post_forum_konu_ekle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'baslik' => 'required|max:50',
            'icerik' => 'required',
            'forum' => 'required'
        ]);
        if ($validator->fails()) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Hatalı Kayıt']);
        }
        try {
            $tarih = Carbon::now();
            $slug = str_slug($request->baslik) . '-' . $tarih;
            $user = Auth::User()->id;
            $request->merge(['slug' => $slug, 'yazar' => $user]);
            ForumListe::create($request->all());
            return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Yeni Bir Konu Açtınız.']);
        } catch (\Exception $e) {
            return response(['durum' => 'success', 'baslik' => 'Hatalı', 'icerik' => 'Hatalı Kayıt']);
        }
    }

    public function post_forum_konu_yorum(Request $request, $anabaslik, $slug)
    {

        if (Auth::check()) {
            $validator = Validator::make($request->all(), [
                'icerik' => 'required',
            ]);
        } else {
            redirect('/giris-yap');
        }
        if ($validator->fails()) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Hatalı Kayıt']);
        }
        try {
            $forumid=ForumListe::where('slug',$slug)->first()->id;
            $request->merge(['kullanici_id' => Auth::user()->id, 'forum' => $forumid]);
            ForumYorumlar::create($request->all());
            return response(['durum' => 'success', 'baslik' => 'Başarılı', 'icerik' => 'Başarıyla Yorum Yaptınız']);
        } catch (\Exception $e) {
            return response(['durum' => 'error', 'baslik' => 'Hatalı', 'icerik' => 'Hatalı Veri Girdiniz.']);
        }

    }
}
