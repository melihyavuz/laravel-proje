<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    protected $table='forum';
    protected $fillable=['baslik','kisa_aciklama','slug'];
    public function forum_liste(){
        return $this->hasMany('App\ForumListe','forum','id');
    }

}
