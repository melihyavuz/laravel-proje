<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ayarlar extends Model
{
    protected $table="ayarlar";
    protected $fillable=["ayar_logo","ayar_siteurl","ayar_title","ayar_description","ayar_keywords","ayar_author","ayar_tel","ayar_gsm","ayar_faks","ayar_mail","ayar_adres","ayar_ilce","ayar_il","ayar_google","ayar_recaptcha","ayar_googlemap","ayar_analystic","ayar_facebook","ayar_twitter","ayar_youtube","ayar_instagram","ayar_smtphost","ayar_smtpuser","ayar_smtppassword","ayar_smtpport"];
}
