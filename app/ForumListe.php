<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumListe extends Model
{
    protected $table='forum-liste';
    protected $fillable=['baslik','icerik','slug','etiketler','yazar','forum'];
    public function forumm(){
        return $this->hasOne('App\Forum','id','forum');
    }
    public function user(){
        return $this->hasOne('App\User','id','yazar');
    }
    public function yorumlar(){
        return $this->hasMany('App\ForumYorumlar','forum','id');
    }
}
