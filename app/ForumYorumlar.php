<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumYorumlar extends Model
{
    protected $table='forum_yorumlar';
    protected $fillable=['ust_yorum','kullanici_id','forum','icerik'];
    public function ForumKonu(){
        return $this->hasMany('App\ForumListe','id','forum');
    }
    public function User(){
        return $this->hasOne('App\User','id','kullanici_id');
    }
    public function children(){
        return $this->hasMany('App\ForumYorumlar','ust_yorum');
    }
}
