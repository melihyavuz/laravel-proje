<?php

use Illuminate\Auth\Middleware\Authenticate;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => '/'], function () {
    Route::get('/', 'HomeGetController@get_index');
    Route::get('/index', 'HomeGetController@get_index_yonlendir');
    Route::get('/anasayfa', 'HomeGetController@get_index_yonlendir');
    Route::get('/home', 'HomeGetController@get_index_yonlendir');
    Route::get('/iletisim', 'HomeGetController@get_iletisim');
    Route::get('/hakkimizda', 'HomeGetController@get_hakkimizda');
    Route::get('/blog', 'HomeGetController@get_blog');
    Route::get('/forum', 'HomeGetController@get_forum');
    Route::get('/forum/konu-ekle', 'HomeGetController@get_forum_konu_ekle');
    Route::post('/forum/konu-ekle', 'HomePostController@post_forum_konu_ekle');
    Route::get('/forum/forum-liste/{ana_konu}/{slug}', 'HomeGetController@get_forum_detay');
    Route::get('/forum/forum-liste/{slug}', 'HomeGetController@get_forum_liste');
    Route::post('/forum/forum-liste/{ana_konu}/{slug}', 'HomePostController@post_forum_konu_yorum');
    Route::get('/forum/author/{author}', 'HomeGetController@get_author_forum');
    Route::get('/forum/tag/{tag}', 'HomeGetController@get_tag_forum');
    Route::get('/giris-yap', 'HomeGetController@get_login');
    Route::get('/cikis-yap', 'HomeGetController@get_logout');
    Route::get('/blog/yazar/{yazar}', 'HomeGetController@get_blog_yazar');
    Route::get('/blog/tags/{tag}', 'HomeGetController@get_blog_tag');
    Route::get('/blog/{slug}', 'HomeGetController@get_blog_icerik')->where('slug', '^[a-zA-Z0-9-_\/]+$');
    Route::post('/blog/{slug}', 'HomePostController@post_blog_yorum')->where('slug', '^[a-zA-Z0-9-_\/]+$');
});
Route::group(['prefix' => 'admin', 'middleware' => 'Admin'], function () {
    Route::get('/', 'AdminGetController@get_index')->middleware('auth');
    Route::get('/ayarlar', 'AdminGetController@get_ayarlar')->middleware('auth');
    Route::post('/ayarlar', 'AdminPostController@post_ayarlar')->middleware('auth');
    Route::get('/hakkimizda', 'AdminGetController@get_hakkimizda')->middleware('auth');
    Route::post('/hakkimizda', 'AdminPostController@post_hakkimizda')->middleware('auth');

    Route::group(['prefix' => 'blog'], function () {
        Route::get('/', 'AdminGetController@get_blog')->middleware('auth');
        Route::get('/blog-ekle', 'AdminGetController@get_blog_ekle')->middleware('auth');
        Route::post('/blog-ekle', 'AdminPostController@post_blog_ekle')->middleware('auth');
        Route::post('/', 'AdminPostController@post_blog_sil')->middleware('auth');
        Route::get('/blog-duzenle/{slug}', 'AdminGetController@get_blog_duzenle')->middleware('auth');
        Route::post('/blog-duzenle/{slug}', 'AdminPostController@post_blog_duzenle')->middleware('auth');
        Route::get('/kategori-ekle', 'AdminGetController@get_kategori_ekle')->middleware('auth');
        Route::post('/kategori-ekle', 'AdminPostController@post_kategori_ekle')->middleware('auth');
        Route::get('/kategori', 'AdminGetController@get_kategoriler')->middleware('auth');
        Route::post('/kategori', 'AdminPostController@post_kategori_sil')->middleware('auth');
    });
    Route::group(['prefix' => 'forum'], function () {
        Route::get('/', 'AdminGetController@get_forum_liste')->middleware('auth');
        Route::get('/forum-ekle', 'AdminGetController@get_forum')->middleware('auth');
        Route::post('/forum-ekle', 'AdminPostController@post_forum')->middleware('auth');
    });

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


