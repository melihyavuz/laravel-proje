@extends('backend.app')
@section('icerik')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Blog Ekle</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="post" id="form" data-parsley-validate
                                          class="form-horizontal form-label-left">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Resimler
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="file" required multiple name="resimler[]"
                                                       class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kategori
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="form-control" name="kategori">
                                                    <option value="0">Kategori Seçiniz</option>
                                                    @foreach($kategoriler as $kategori)
                                                        <option value="{{$kategori->id}}">{{$kategori->ad}}</option>
                                                        @foreach($kategori->children as $altkategori)
                                                            <option value="{{$altkategori->id}}">{{$kategori->ad}}-->{{$altkategori->ad}}</option>
                                                            @foreach($altkategori->children as $altaltkategori)
                                                                <option value="{{$altaltkategori->id}}">{{$kategori->ad}}-->{{$altkategori->ad}}-->{{$altaltkategori->ad}}</option>
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        {{Form::bsText('baslik','Başlık','',['required'=>'required'])}}
                                        {{Form::bsText('kisaicerik','Kısa Açıklama','',['required'=>'required'])}}
                                        {{Form::bsText('etiketler','Etiketler','',['required'=>'required'])}}
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">İçerik
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea name="icerik"  class="form-control col-md-7 col-xs-12 ckeditor"
                                                          cols="30" rows="10" required></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Kaydet
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/messages_tr.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script src="/ckeditor/ckeditor.js"></script>
    <script src="/ckeditor/config.js"></script>
    <script src="/js/fontawesome.min.js"></script>
    <script src="/js/all.min.js"></script>
    <script>
        $(document).ready(function () {
            $('form').validate();
            $('form').ajaxForm({
                beforeSubmit: function () {
                    swal({
                        html: '<i class="fa fa-spinner fa-pulse"></i><span class="sr-only">Loading...</span>',
                        text: 'Yükleniyor lütfen bekleyiniz...',
                        showConfirmButton: false
                    })
                },
                beforeSerialize: function () {
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                },
                success: function (response) {
                    swal(
                        response.baslik,
                        response.icerik,
                        response.durum
                    );
                }
            });

        });
    </script>
@endsection
@section('css')
    <link href="/css/sweetalert.css" rel="stylesheet">
    <link href="/css/all.min.css" rel="stylesheet">
    <link href="/css/fontawesome.min.css" rel="stylesheet">
@endsection