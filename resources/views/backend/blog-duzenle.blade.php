@extends('backend.app')
@section('icerik')
    @php
        $sira=1;
    @endphp
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Blog Düzenle</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="row">
                                @foreach($resimler=Storage::disk('uploads')->files('img/blog/'.$bloglar->slug)  as $resim )
                                    <div class="col-md-55" id="resim{{$sira}}">
                                        <div class="thumbnail h-100">
                                            <div class="image view view-first h-100">
                                                <img class="h-142" style="width: 100%; display: block;"
                                                     src="/uploads/{{$resim}}"
                                                     alt="image"/>
                                                <div class="mask h-100">
                                                    <div class="tools tools-bottom">
                                                        <form action="" method="post" id="form">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="resim" value="{{$resim}}">
                                                            <a onclick="sil('{{$sira}}','{{$resim}}')"
                                                               class="btn btn-danger"><i class="fa fa-times"></i>
                                                            </a>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        $sira++;
                                    @endphp
                                @endforeach
                            </div>
                            <form method="post" id="form" data-parsley-validate
                                  class="form-horizontal form-label-left">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Yeni Resim
                                        Ekle
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" multiple name="resimler[]"
                                               class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                {{Form::bsText('baslik','Başlık',$bloglar->baslik,['required'=>'required'])}}
                                {{Form::bsText('kisaicerik','Kısa Açıklama',$bloglar->kisaicerik,['required'=>'required'])}}
                                {{Form::bsText('etiketler','Etiketler',$bloglar->etiketler,['required'=>'required'])}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">İçerik
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea name="icerik" class="form-control col-md-7 col-xs-12 ckeditor"
                                                          cols="30" rows="10" required>{{$bloglar->icerik}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <input type="hidden" name="sira" value="{{$sira}}">
                                        <button type="submit" class="btn btn-success">Kaydet
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/messages_tr.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script src="/ckeditor/ckeditor.js"></script>
    <script src="/ckeditor/config.js"></script>
    <script>
        function sil(r, resim) {
            var sira = r;
            swal({
                title: "Silmek istediğinizden emin misiniz?",
                text: "Sildiğinizde geri dönüşüm olmayacaktır.",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: 'İptal',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Evet, Sil!'

            }, function () {
                // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: "Post",
                    url: '',
                    data: {
                        'resim': resim,
                        "_token": "{{ csrf_token() }}",
                    },

                    success: function (response) {
                        if (response.durum == 'success') {
                            $("#resim" + sira).fadeOut("slow");
                        }
                        swal(
                            response.baslik,
                            response.icerik,
                            response.durum
                        );

                    }

                })

            })
        }
    </script>
    <script>
        $(document).ready(function () {
            $('form').validate();
            $('form').ajaxForm({
                beforeSerialize: function () {
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                },
                success: function (response) {
                    swal(
                        response.baslik,
                        response.icerik,
                        response.durum
                    );
                }
            });

        });
    </script>
@endsection
@section('css')
    <link href="/css/sweetalert.css" rel="stylesheet">

@endsection