@extends('backend.app')
@section('icerik')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>General Elements</h3>
                </div>

            </div>

            <div class="clearfix"></div>
            <div class="">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel border-0">
                        <div class="x_content">
                            <form method="post" id="form" data-parsley-validate
                                  class="form-horizontal form-label-left">
                                {{csrf_field()}}
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#genel_ayarlar" id="home-tab"
                                                                                  role="tab"
                                                                                  data-toggle="tab"
                                                                                  aria-expanded="true">Genel Ayarlar</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#iletisim_ayarlari" role="tab"
                                                                            id="profile-tab"
                                                                            data-toggle="tab"
                                                                            aria-expanded="false">İletişim Ayarları</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#sosyal_medya" role="tab"
                                                                            id="profile-tab2"
                                                                            data-toggle="tab"
                                                                            aria-expanded="false">Sosyal Medya</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#google_api" role="tab"
                                                                            id="profile-tab2"
                                                                            data-toggle="tab"
                                                                            aria-expanded="false">Google API</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#mail_ayarlari" role="tab"
                                                                            id="profile-tab2"
                                                                            data-toggle="tab"
                                                                            aria-expanded="false">Mail Ayarları</a>
                                        </li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="genel_ayarlar"
                                             aria-labelledby="home-tab">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="x_panel">
                                                        <div class="x_content">
                                                            <br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mevcut Logo</span>
                                                                </label>
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <img src="/uploads/img/{{$ayarlar->ayar_logo}}" class="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Site Logo
                                                                </label>
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <input type="file" id="first-name" name="ayar_logo"  class="form-control col-md-7 col-xs-12">
                                                                    <input type="hidden" id="first-name" name="eski_logo" value="{{$ayarlar->ayar_logo}}"  class="form-control col-md-7 col-xs-12">
                                                                </div>
                                                            </div>
                                                            {{Form::bsText('ayar_title','Site Başlığı',$ayarlar->ayar_title)}}
                                                            {{Form::bsText('ayar_keywords','Site Anahtar Kelimeler',$ayarlar->ayar_keywords)}}
                                                            {{Form::bsText('ayar_description','Site Açıklaması',$ayarlar->ayar_description)}}
                                                            {{Form::bsText('ayar_siteurl','Site Adresi',$ayarlar->ayar_siteurl)}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="iletisim_ayarlari"
                                             aria-labelledby="profile-tab">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="x_panel">
                                                        <div class="x_content">
                                                            <br/>
                                                            {{Form::bsText('ayar_tel','Telefon',$ayarlar->ayar_tel)}}
                                                            {{Form::bsText('ayar_gsm','Gsm',$ayarlar->ayar_gsm)}}
                                                            {{Form::bsText('ayar_faks','Faks',$ayarlar->ayar_faks)}}
                                                            {{Form::bsText('ayar_mail','Mail Adresi',$ayarlar->ayar_mail)}}
                                                            {{Form::bsText('ayar_adres','Adres',$ayarlar->ayar_adres)}}
                                                            {{Form::bsText('ayar_il','İl',$ayarlar->ayar_il)}}
                                                            {{Form::bsText('ayar_ilce','İlçe',$ayarlar->ayar_ilce)}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="sosyal_medya"
                                             aria-labelledby="profile-tab">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="x_panel">
                                                        <div class="x_content">
                                                            <br/>
                                                            {{Form::bsText('ayar_facebook','Facebook',$ayarlar->ayar_facebook)}}
                                                            {{Form::bsText('ayar_twitter','Twitter',$ayarlar->ayar_twitter)}}
                                                            {{Form::bsText('ayar_instagram','İnstagram',$ayarlar->ayar_instagram)}}
                                                            {{Form::bsText('ayar_youtube','Youtube',$ayarlar->ayar_youtube)}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="google_api"
                                             aria-labelledby="profile-tab">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="x_panel">
                                                        <div class="x_content">
                                                            <br/>
                                                            {{Form::bsText('ayar_google','Google Hesabı',$ayarlar->ayar_google)}}
                                                            {{Form::bsText('ayar_recaptcha','Google Recaptcha',$ayarlar->ayar_recaptcha)}}
                                                            {{Form::bsText('ayar_googlemap','Google Maps',$ayarlar->ayar_googlemap)}}
                                                            {{Form::bsText('ayar_analystic','Google Analystic',$ayarlar->ayar_analystic)}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="mail_ayarlari"
                                             aria-labelledby="profile-tab">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="x_panel">
                                                        <div class="x_content">
                                                            <br/>
                                                            {{Form::bsText('ayar_smtpuser','Kullanıcı Adı',$ayarlar->ayar_smtpuser)}}
                                                            {{Form::bsPassword('ayar_smtppassword','Şifre',$ayarlar->ayar_smtppassword)}}
                                                            {{Form::bsText('ayar_smtphost','SMTP Host',$ayarlar->ayar_smtphost)}}
                                                            {{Form::bsText('ayar_smtpport','SMTP Port',$ayarlar->ayar_smtpport)}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success">Kaydet
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/messages_tr.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script>
        $(document).ready(function () {
            $('form').validate();
            $('form').ajaxForm({
                beforeSubmit: function () {

                },
                success: function (response) {
                    swal(
                        response.baslik,
                        response.icerik,
                        response.durum
                    );
                }
            });

        });
    </script>
@endsection
@section('css')
    <link href="/css/sweetalert.css" rel="stylesheet">
@endsection
