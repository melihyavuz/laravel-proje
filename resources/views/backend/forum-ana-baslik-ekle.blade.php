@extends('backend.app')
@section('icerik')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Blog Ekle</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="post" id="form" data-parsley-validate
                                          class="form-horizontal form-label-left">
                                        {{csrf_field()}}

                                        {{Form::bsText('baslik','Başlık','',['required'=>'required'])}}
                                        {{Form::bsText('kisa_aciklama','Kısa Açıklama','',['required'=>'required'])}}


                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Kaydet
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/messages_tr.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script>
        $(document).ready(function () {
        $('form').validate();
        $('form').ajaxForm({
            success:function (response) {
                swal(
                    response.baslik,
                    response.icerik,
                    response.durum
                );
            }
        })
        })
    </script>
@endsection
@section('css')
    <link href="/css/sweetalert.css" rel="stylesheet">
@endsection