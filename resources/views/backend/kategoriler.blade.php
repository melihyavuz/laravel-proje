@extends('backend.app')
@section('icerik')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Kategori Ekle</h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <table class="table" id="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kategori</th>
                                    <th>Sil</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($kategoriler as $kategori)
                                    <tr>
                                        <th scope="row">{{$kategori->id}}</th>
                                        <td>{{$kategori->ad}}</td>
                                        <td>
                                            <button onclick="sil(this,'{{$kategori->id}}')" class="btn btn-danger">Sil
                                            </button>
                                        </td>
                                    </tr>
                                    @foreach($kategori->children as $altkategori)
                                        <tr>
                                            <th scope="row">{{$altkategori->id}}</th>
                                            <td>{{$kategori->ad}}-->{{$altkategori->ad}}</td>
                                            <td>
                                                <button onclick="sil(this,'{{$altkategori->id}}')"
                                                        class="btn btn-danger">Sil
                                                </button>
                                            </td>

                                        </tr>
                                        @foreach($altkategori->children as $altaltkategori)
                                            <tr>
                                                <th scope="row">{{$altaltkategori->id}}</th>
                                                <td>{{$kategori->ad}}-->{{$altkategori->ad}}
                                                    -->{{$altaltkategori->ad}}</td>
                                                <td>
                                                    <button onclick="sil(this,'{{$altaltkategori->id}}')"
                                                            class="btn btn-danger">Sil
                                                    </button>
                                                </td>

                                            </tr>

                                        @endforeach
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/messages_tr.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script>
        function sil(r, id) {
            var sira = r.parentNode.parentNode.rowIndex;
            swal({
                title: 'Silmek istediğinize emin misiniz?',
                text: "Sildiğinizde geri dönüşü olmayacaktır!",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'İptal',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Evet, Sil!'
            }, function () {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: "Post",
                    url: '',
                    data: {
                        'id': id,
                        '_token': CSRF_TOKEN
                    },

                    success: function (response) {

                        if (response.durum == 'success') {
                            document.getElementById("table").deleteRow(sira);
                        }
                        swal(
                            response.baslik,
                            response.icerik,
                            response.durum
                        );

                    }

                })
            })
        }
    </script>
@endsection
@section('css')
    <link href="/css/sweetalert.css" rel="stylesheet">
@endsection