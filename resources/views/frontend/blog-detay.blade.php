@extends('frontend.app')
@section('icerik')
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="/">Anasayfa</a></li>
                            <li class="active">{{$blog->baslik}}</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h1>{{$blog->baslik}}</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">
                <div class="col-lg-9">
                    <div class="blog-posts single-post">

                        <article class="post post-large blog-single-post">
                            <div class="post-image">
                                <div class="owl-carousel owl-theme" data-plugin-options="{'items':1}">
                                    @foreach($resimler=Storage::disk('uploads')->files('img/blog/'.$blog->slug) as $resim)
                                        <div>
                                            <div class="img-thumbnail d-block">
                                                <img class="img-fluid" src="/uploads/{{$resim}}" alt="">
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="post-date">
                                @php(setlocale(LC_TIME,'turkish') )
                                <span class="day">{{$blog->created_at->formatLocalized('%d')}}</span>
                                <span class="month">{{$blog->updated_at->formatLocalized('%b')}}</span>
                            </div>

                            <div class="post-content">

                                <h2><a>{{$blog->baslik}}</a></h2>

                                <div class="post-meta">
                                    <span><i class="fa fa-user"></i> Blog <a
                                                href="/blog/yazar/{{$blog->user->slug}}-{{$blog->yazar}}">{{$blog->user->name}}</a> tarafından oluşturuldu. </span>
                                    <span><i class="fa fa-tag"></i>
                                        @php($tags=explode(',',$blog->etiketler))
                                        @foreach($tags as $tag)
                                            <a href="/blog/tags/{{$tag}}-{{$blog->id}}">{{$tag}}</a></span>
                                    @endforeach
                                    <span><i class="fa fa-comments"></i> <a>{{$blog->yorumlar->count()}} Yorum</a></span>
                                </div>
                                <blockquote>{{$blog->kisaicerik}}</blockquote>

                                {{$blog->icerik}}


                                <div class="post-block post-author clearfix">
                                    <h3 class="heading-primary"><i class="fa fa-user"></i>Konu Sahibi</h3>
                                    <div class="img-thumbnail d-block">
                                        <a href="blog-post.html">
                                            <img src="img/avatars/avatar.jpg" alt="">
                                        </a>
                                    </div>
                                    <p><strong class="name"><a href="#">{{$blog->user->name}}</a></strong></p>
                                    <p>{{$blog->user->description}} </p>
                                </div>

                                <div class="post-block post-comments clearfix">
                                    <h3 class="heading-primary"><i class="fa fa-comments"></i>Yorum ({{$blog->yorumlar->count()}})</h3>

                                    <ul class="comments">
                                        <li id="yorumlar"></li>
                                        @foreach($blog->yorumlar->where('ust_yorum','0') as $yorum)
                                            <li>
                                                <div class="comment">
                                                    <div class="img-thumbnail d-none d-sm-block">
                                                        <img class="avatar" alt="" src="img/avatars/avatar-2.jpg">
                                                    </div>
                                                    <div class="comment-block">
                                                        <div class="comment-arrow"></div>
                                                        <span class="comment-by">

                                                                    @if($yorum->kullanici_id>0)
                                                                <strong> {{$yorum->user->name}}</strong>
                                                            @else
                                                                {{$yorum->isim}}
                                                            @endif
																<span class="float-right">
																	<span> <a onclick="altyorum({{$yorum->id}})"><i
                                                                                    class="fa fa-reply"></i> Reply</a></span>
																</span>
															</span>
                                                        <p>{{$yorum->icerik}}</p>
                                                        @php($zaman=$yorum->created_at)
                                                        @php($zaman->setLocale('tr'))
                                                        <span class="date float-right">{{$zaman->diffForHumans()}}</span>
                                                    </div>
                                                </div>

                                                <ul class="comments reply">
                                                    @foreach($yorum->children as $altyorum)
                                                        <li>
                                                            <div class="comment">
                                                                <div class="img-thumbnail d-none d-sm-block">
                                                                    <img class="avatar" alt=""
                                                                         src="img/avatars/avatar-3.jpg">
                                                                </div>
                                                                <div class="comment-block">
                                                                    <div class="comment-arrow"></div>
                                                                    <span class="comment-by">
																		<strong>
                                                                            @if($altyorum->kullanici_id>0)
                                                                                {{$altyorum->user->name}}
                                                                            @else
                                                                                {{$altyorum->isim}}
                                                                            @endif
                                                                        </strong>
                                                                        <span class="float-right">
																			<span> <a href="#"><i
                                                                                            class="fa fa-reply"></i> Reply</a></span>
																		</span>
																	</span>
                                                                    <p>{{$altyorum->icerik}}</p>
                                                                    <span class="date float-right">{{$altyorum->created_at->diffForHumans()}}</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endforeach
                                    </ul>

                                </div>

                                <div class="post-block post-leave-comment">
                                    <h3 class="heading-primary">Yorum Bırak</h3>

                                    <form method="post" id="form" data-parsley-validate>
                                        {{csrf_field()}}
                                        <div id="altyorum"></div>
                                        @if(!\Illuminate\Support\Facades\Auth::check())
                                            <div class="form-row">
                                                <div class="form-group col-lg-6">
                                                    <label>İsminiz *</label>
                                                    <input type="text" value="" maxlength="100" class="form-control"
                                                           name="isim" id="name">
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>Mail Adresiniz *</label>
                                                    <input type="email" value="" maxlength="100" class="form-control"
                                                           name="mail" id="email">
                                                </div>
                                            </div>
                                        @endif

                                        <div class="form-row">
                                            <div class="form-group col">
                                                <label>Mesajınız *</label>
                                                <textarea maxlength="5000" rows="10" class="form-control" name="icerik"
                                                          id="comment"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <input type="submit" value="Yorumu Paylaş" class="btn btn-primary btn-lg"
                                                       data-loading-text="Loading...">
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </article>

                    </div>
                </div>

                @include('frontend.blog-side-bar')
            </div>

        </div>

    </div>

@endsection
@section('js')
    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/messages_tr.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script>
        function altyorum(id) {
            var hidden = '<input type="hidden" value="' + id + '" name="ust_yorum">'
            document.getElementById('altyorum').innerHTML = hidden;
        }

        $(document).ready(function () {
            $('form').validate();
            $('form').ajaxForm({
                success: function (response) {
                    swal(
                        response.baslik,
                        response.icerik,
                        response.durum
                    );
                    if (response.durum == 'success') {
                        var isim = document.getElementById('name').value;
                        var icerik = document.getElementById('comment').value;
                        var mesaj = '<div class="comment">' +
                            '<div class="img-thumbnail">' +
                            '</div>' +
                            '<div class="comment-block">' +
                            '<div class="comment-arrow">' +
                            '</div>' +
                            '<span class="comment-by">' +
                            '<strong>' + isim + '</strong>' +
                            '<span class="pull-right">' +
                            '<span>' +
                            '<a href="#"><i class="fa fa-reply"></i>Reply</a>' +
                            '</span>' +
                            '</span>' +
                            '</span>' +
                            '<p>' + icerik + '</p>' +
                            '<span class="date pull-right">Şimdi</span>' +
                            '</div>' +
                            '</div>';
                        document.getElementById('yorumlar').innerHTML = mesaj;
                    }

                }
            });
        });
    </script>
@endsection
@section('css')
    <link href="/css/sweetalert.css" rel="stylesheet">
@endsection