@extends('frontend.app')
@section('icerik')
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="/">Anasayfa</a></li>
                            <li class="active"><a href="/forum">Forum</a></li>

                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h1>Forum</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="blog-posts">
                        @foreach($konular as $konu)
                            <article class="post post-large">
                                <div class="post-content">

                                    <hr>
                                    <div class="post-date">
                                        @php(setlocale(LC_TIME, "turkish"))
                                        <span class="day">{{$konu->created_at->formatLocalized('%d')}}</span>
                                        <span class="month">{{$konu->created_at->formatLocalized('%b')}}</span>
                                    </div>

                                    <div class="post-content">

                                        <h2>
                                            <a href="/forum/forum-liste/{{$konu->forumm->slug}}/{{$konu->slug}}">{{$konu->baslik}}</a>
                                        </h2>

                                        <div class="post-meta">
                                            <span><i class="fa fa-user"></i> Konu <a href="/forum/author/{{$konu->user->slug}}">{{$konu->user->name}}</a> tarafından oluşturuldu. </span>
                                            <span><i class="fa fa-tag"></i>
                                                @php($tags=explode(',',$konu->etiketler))
                                                @foreach($tags as $tag)
                                                <a href="/forum/tag/{{$tag}}">{{$tag}}</a>
                                                @endforeach

                                            </span>
                                            <span><i class="fa fa-comments"></i> {{$konu->yorumlar->count()}} Yorum</span>
                                            <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a
                                                        href="/forum/forum-liste/{{$konu->forumm->slug}}/{{$konu->slug}}"
                                                        class="btn btn-xs btn-primary">Daha Fazla</a></span>
                                        </div>

                                    </div>

                                </div>
                            </article>
                        @endforeach
                        <ul class="pagination float-right">
                            <li class="page-item"><a class="page-link" href="#">«</a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">»</a></li>
                        </ul>

                    </div>
                </div>
                @include('frontend.forum-sidebar')
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection
@section('css')
@endsection