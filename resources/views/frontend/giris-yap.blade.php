@extends('frontend.app')
@section('icerik')
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Pages</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h1>Login</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">
                <div class="col">

                    <div class="featured-boxes">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="featured-box featured-box-primary text-left mt-5">
                                    <div class="box-content">
                                        <h4 class="heading-primary text-uppercase mb-3">BEN ZATEN ÜYEYİM</h4>
                                        <form action="{{ route('giris-yap') }}" id="frmSignIn" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <label>Email Adresi</label>
                                                    <input type="text" name="email"  value="" class="form-control form-control-lg">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <a class="float-right" href="#">(Lost Password?)</a>
                                                    <label>Şifre</label>
                                                    <input type="password" name="password" value="" class="form-control form-control-lg">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Beni Hatırla
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <input type="submit" value="Giriş Yap" class="btn btn-primary float-right mb-5" data-loading-text="Loading...">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="featured-box featured-box-primary text-left mt-5">
                                    <div class="box-content">
                                        <h4 class="heading-primary text-uppercase mb-3">ÜYE OLMAK İÇİN</h4>
                                        <form action="{{ route('register') }}"id="frmSignUp" method="post">
                                            {{csrf_field()}}
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <label>İsim*</label>
                                                    <input type="text" name="name" value="" class="form-control form-control-lg">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <label>Email*</label>
                                                    <input type="text" name="email" value="" class="form-control form-control-lg">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-lg-6">
                                                    <label>Şifre</label>
                                                    <input type="password" value="" name="password" class="form-control form-control-lg">
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>Şifre Onayla</label>
                                                    <input type="password" value="" name="password_confirmation" class="form-control form-control-lg">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="submit" value="Kayıt Ol" class="btn btn-primary float-right mb-5" data-loading-text="Loading...">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
@section('js')
@endsection
@section('css')
@endsection

