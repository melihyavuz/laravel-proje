@extends('frontend.app')
@section('icerik')
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="/">Anasayfa</a></li>
                            <li class="active"><a href="/forum">Forum</a></li>
                            <li class="active">{{$forum->baslik}}</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h1>{{$forum->baslik}}</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">
                <div class="col-lg-9">
                    <div class="blog-posts single-post">

                        <article class="post post-large blog-single-post">
                            <div class="post-date">
                                @php(setlocale(LC_TIME, "turkish"))
                                <span class="day">{{$forum->created_at->formatLocalized('%d')}}</span>
                                <span class="month">{{$forum->created_at->formatLocalized('%b')}}</span>
                            </div>

                            <div class="post-content">

                                <h2><a>{{$forum->baslik}}</a></h2>

                                <div class="post-meta">
                                    <span><i class="fa fa-user"></i> Konu <a
                                                href="/forum/author/{{$forum->user->slug}}">{{$forum->user->name}}</a> tarafından oluşturuldu.</span>
                                    <span><i class="fa fa-tag"></i>
                                        @php($tags=explode(',',$forum->etiketler))
                                        @foreach($tags as $tag)
                                            <a href="/forum/tag/{{$tag}}">{{$tag}}</a>
                                        @endforeach
                                    </span>
                                    <span><i class="fa fa-comments"></i> <a
                                                >{{$forum->yorumlar->count()}} Yorum</a></span>
                                </div>

                                <p>{{$forum->icerik}}</p>


                                <div class="post-block post-author clearfix">
                                    <h3 class="heading-primary"><i class="fa fa-user"></i>Konu Sahibi</h3>
                                    <div class="img-thumbnail d-block">
                                        <a href="blog-post.html">
                                            <img src="img/avatars/avatar.jpg" alt="">
                                        </a>
                                    </div>
                                    <p><strong class="name"><a href="#">{{$forum->user->name}}</a></strong></p>
                                    <p>{{$forum->user->description}}</p>
                                </div>

                                <div class="post-block post-comments clearfix">
                                    <h3 class="heading-primary"><i
                                                class="fa fa-comments"></i>Yorumlar({{$forum->yorumlar->count()}})</h3>

                                    <ul class="comments">
                                        <li id="yorumlar"></li>
                                        @foreach($forum->yorumlar->where('ust_kategori',0) as  $for )
                                            <li>
                                                <div class="comment">
                                                    <div class="img-thumbnail d-none d-sm-block">
                                                        <img class="avatar" alt="" src="img/avatars/avatar-2.jpg">
                                                    </div>
                                                    <div class="comment-block">
                                                        <div class="comment-arrow"></div>
                                                        <span class="comment-by">
																<strong>{{$for->user->name}}</strong>
																<span class="float-right">
																	<span> <a onclick="altyorum({{$for->id}})"><i
                                                                                    class="fa fa-reply"></i> Yanıtla</a></span>
																</span>
															</span>
                                                        <p>{{$for->icerik}}</p>
                                                        @php($zaman=$for->created_at)
                                                        @php($zaman->setLocale('tr'))
                                                        <span class="date float-right">{{$zaman->diffForHumans()}}</span>
                                                    </div>
                                                </div>

                                                <ul class="comments reply">
                                                    @foreach($for->children as $altyorum)
                                                        <li>
                                                            <div class="comment">
                                                                <div class="img-thumbnail d-none d-sm-block">
                                                                    <img class="avatar" alt=""
                                                                         src="img/avatars/avatar-3.jpg">
                                                                </div>
                                                                <div class="comment-block">
                                                                    <div class="comment-arrow"></div>
                                                                    <span class="comment-by">
																		<strong>{{$altyorum->user->name}}</strong>
																		<span class="float-right">
																			<span> <a href="#"><i
                                                                                            class="fa fa-reply"></i> Reply</a></span>
																		</span>
																	</span>
                                                                    <p>{{$altyorum->icerik}}</p>
                                                                    @php($zaman=$altyorum->created_at)
                                                                    @php($zaman->setLocale('tr'))
                                                                    <span class="date float-right">{{$zaman->diffForHumans()}}</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="post-block post-leave-comment">
                                    <h3 class="heading-primary">Bir Yorum Bırak</h3>

                                    <form action="" method="post" id="form">
                                        {{csrf_field()}}
                                        <div id="altyorum"></div>
                                        @if(\Illuminate\Support\Facades\Auth::check())
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <label>Mesajınız*</label>
                                                    <textarea maxlength="5000" rows="10" class="form-control"
                                                              name="icerik" id="comment"></textarea>
                                                </div>
                                            </div>
                                        @else
                                            <span style="color:red">Yorum yapabilmeniz için üye girişi yapmanız gerekmektedir.</span>
                                        @endif
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <input type="submit" value="Yorumu Gönder"
                                                       class="btn btn-primary btn-lg"
                                                       data-loading-text="Loading...">
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </article>

                    </div>
                </div>

                @include('frontend.forum-sidebar')
            </div>

        </div>

    </div>


        @if(\Illuminate\Support\Facades\Auth::check())
        @php
            $hidden=\Illuminate\Support\Facades\Auth::user()->name;
            @endphp
        <input type="hidden" value="{{$hidden}}" maxlength="100" class="form-control"
               name="name" id="name">
        @endif

@endsection
@section('js')
    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/messages_tr.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script>
        function altyorum(id) {
            var hidden = '<input type="hidden" value="' + id + '" name="ust_yorum">';
            document.getElementById('altyorum').innerHTML = hidden;
        }

        $(document).ready(function () {
            $('form').validate();
            $('form').ajaxForm({
                success: function (response) {
                    swal(
                        response.baslik,
                        response.icerik,
                        response.durum
                    );
                    if (response.durum == 'success') {
                        var isim = document.getElementById('name').value;
                        var icerik = document.getElementById('comment').value;
                        var mesaj = '<div class="comment">' +
                            '<div class="img-thumbnail">' +
                            '</div>' +
                            '<div class="comment-block">' +
                            '<div class="comment-arrow">' +
                            '</div>' +
                            '<span class="comment-by">' +
                            '<strong>' + isim + '</strong>' +
                            '<span class="pull-right">' +
                            '<span>' +
                            '<a href="#"><i class="fa fa-reply"></i>Reply</a>' +
                            '</span>' +
                            '</span>' +
                            '</span>' +
                            '<p>' + icerik + '</p>' +
                            '<span class="date pull-right">Şimdi</span>' +
                            '</div>' +
                            '</div>';
                        document.getElementById('yorumlar').innerHTML = mesaj;
                    }
                }
            });
        });
    </script>
@endsection
@section('css')
    <link href="/css/sweetalert.css" rel="stylesheet">
@endsection