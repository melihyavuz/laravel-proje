<div class="col-lg-3">
    <aside class="sidebar">

        <a href="forum/konu-ekle">
            <button class="btn btn-primary"><i class="fa fa-plus mr-3"></i>Yeni Konu Ekle</button>
        </a>

        <hr>

        <h4 class="heading-primary">Kategoriler</h4>
        <ul class="nav nav-list flex-column mb-5">
            @foreach($categories as $category)
                <li class="nav-item">
                    <a class="nav-link active" href="/forum/forum-liste/{{$category->slug}}">{{$category->baslik}} (4)</a>
                    <ul>
                        @foreach($category->forum_liste as $altcategory)
                            <li class="nav-item"><a class="nav-link" href="/forum/forum-liste/{{$category->slug}}/{{$altcategory->slug}}">{{$altcategory->baslik}}</a></li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>

        <div class="tabs mb-5">
            <ul class="nav nav-tabs">
                <li class="nav-item active"><a class="nav-link" href="#popularPosts"
                                               data-toggle="tab"><i class="fa fa-star"></i> Popular</a>
                </li>
                <li class="nav-item"><a class="nav-link" href="#recentPosts"
                                        data-toggle="tab">Recent</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="popularPosts">
                    <ul class="simple-post-list">
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="blog-post.html">
                                        <img src="img/blog/blog-thumb-1.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="blog-post.html">Nullam Vitae Nibh Un Odiosters</a>
                                <div class="post-meta">
                                    Jan 10, 2017
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="blog-post.html">
                                        <img src="img/blog/blog-thumb-2.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="blog-post.html">Vitae Nibh Un Odiosters</a>
                                <div class="post-meta">
                                    Jan 10, 2017
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="blog-post.html">
                                        <img src="img/blog/blog-thumb-3.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="blog-post.html">Odiosters Nullam Vitae</a>
                                <div class="post-meta">
                                    Jan 10, 2017
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane" id="recentPosts">
                    <ul class="simple-post-list">
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="blog-post.html">
                                        <img src="img/blog/blog-thumb-2.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="blog-post.html">Vitae Nibh Un Odiosters</a>
                                <div class="post-meta">
                                    Jan 10, 2017
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="blog-post.html">
                                        <img src="img/blog/blog-thumb-3.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="blog-post.html">Odiosters Nullam Vitae</a>
                                <div class="post-meta">
                                    Jan 10, 2017
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="blog-post.html">
                                        <img src="img/blog/blog-thumb-1.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="blog-post.html">Nullam Vitae Nibh Un Odiosters</a>
                                <div class="post-meta">
                                    Jan 10, 2017
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <hr>

        <h4 class="heading-primary">Biz Kimiz</h4>
        <p><?php $excerpt=substr($hakkimizda->kisa_yazi,0,100); echo $excerpt;?>...<a
                        href="/hakkimizda"
                        class="btn btn-xs btn-primary mt-3">Daha Fazla</a></p>

    </aside>
</div>