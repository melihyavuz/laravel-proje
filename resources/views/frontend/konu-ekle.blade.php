@extends('frontend.app')
@section('icerik')
    <div role="main" class="main">
        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="#">Anasayfa</a></li>
                            <li class="active">Konu Ekle</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h1>Yeni Konu Aç</h1>
                    </div>
                </div>
            </div>
        </section>

        <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->

        <div class="container">

            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <h2 class="mb-3 mt-2"><strong>Yeni</strong> Konu Ekle</h2>

                    <form id="form" action="" method="POST">
                        {{csrf_field()}}
                        <div class="form-row">
                            <div class="form-group col-lg-12">
                                <label>Başlık *</label>
                                <input type="text" value="" data-msg-required="Please enter your name." maxlength="100"
                                       class="form-control" name="baslik" id="name" required>
                            </div>
                            <div class="form-group col-lg-12">
                                <label>Ana Başlık *</label>
                                <select class="form-control" name="forum">
                                    <option value="0">Bir Anabaşlık Seçiniz..</option>
                                    @foreach($anakonular as $anakonu)
                                        <option value="{{$anakonu->id}}">{{$anakonu->baslik}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label>Mesaj *</label>
                                <textarea maxlength="5000" data-msg-required="Please enter your message." rows="10"
                                          class="form-control" name="icerik" id="message" required></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="submit" value="Konu Ekle" class="btn btn-primary btn-lg"
                                       data-loading-text="Loading...">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
@endsection
@section('js')
    <script src="/js/jquery.form.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/messages_tr.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script>
        $(document).ready(function () {
            $('form').validate();
            $('form').ajaxForm({
                success:function (response) {
                    swal(
                        response.baslik,
                        response.icerik,
                        response.durum
                    );
                }
            })
        })
    </script>
@endsection
@section('css')
    <link href="/css/sweetalert.css" rel="stylesheet">
@endsection